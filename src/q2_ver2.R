setwd ('/home/allgoodguys/Documents/Studying/Lund_PhD/z_helping-out/Kerstin_gidlof')
library (lme4)
library (brms)

orig = read.csv ('all.csv')
df = read.csv ('all_preprocessed_coor.csv')
# aggregate(product~store, df, function(x)length(unique(x)))
# sum(df$chosen)
# aggregate (chosen~store+cat, df, sum)
# aggregate (participant~store, df, function(x)length(unique(x)))
# aggregate (chosen~store, df, sum)
# aggregate (chosen~store, df, sum) / aggregate (participant~store, df, function(x)length(unique(x)))
# aggregate (chosen~store, df, sum) / aggregate (participant~store, df, function(x)length(unique(x))) / aggregate(product~store, df, function(x)length(unique(x)))

# how many products did each participant look at?
mean ( aggregate(looked~participant, df, sum)[,2] / aggregate(product~participant, df, function(x)length(unique(x)))[,2] ) # 41%
# how many products did each participant buy?
mean ( aggregate(chosen~participant, df, sum)[,2] / aggregate(product~participant, df, function(x)length(unique(x)))[,2] ) # 1.4%

# 
df = df[,c('looked','chosen','dwelltime','redwells','salience','quality','sales','noFacings','familiarStore','familiarCat','product','participant','pos_x','pos_y','size','shelf')]
df = na.omit (df)
df_looked = df[df$looked==T,]
apply (df, 2, range)
cor (df[,c('dwelltime','redwells','salience','quality','sales','noFacings','pos_x','pos_y','size')])



### Question 2: effects of internal and external factors on customer choice in familiar and unfamiliar environments
# effect of coordinates
mod0 = glmer(chosen~pos_x+I(pos_x^2)+pos_y+I(pos_y^2) + (1|product) + (1|participant), df, family='binomial', nAGQ=0)
summary(mod0)
drop1(mod0, test='Chisq') # no effect of pos_y, and pos_x^2 is marginal

mod1 = glmer(chosen~pos_x+I(pos_x^2) + (1|product) + (1|participant), df, family='binomial', nAGQ=0)
summary(mod1)
drop1(mod1, test='Chisq') # very weak

# control for salience and sales
mod2 = glmer(chosen~pos_x+I(pos_x^2)+pos_y+I(pos_y^2)+salience+sales+noFacings+quality+size + (1|product) + (1|participant), df, family='binomial', nAGQ=0)
summary(mod2)
drop1(mod2, test='Chisq') # inter., pos_y becomes signif with these predictors

mod3 = glmer(chosen~pos_y+I(pos_y^2)+salience+sales*quality + (1|product) + (1|participant), df, family='binomial', nAGQ=0)
summary(mod3)
drop1(mod3, test='Chisq')

# effect of salience
mod_s = glmer(chosen~salience + (1|product) + (1|participant), df, family='binomial', nAGQ=0)
summary(mod_s)
drop1(mod_s, test='Chisq')

## effect of familiarity
mod4 = glmer(chosen~pos_y+I(pos_y^2)+salience+sales+quality*familiarStore + quality*familiarCat + sales:quality + (1|product) + (1|participant), df, family='binomial', nAGQ=0)
summary(mod4)
drop1(mod4, test='Chisq') # no interaction b/w familiarity and quality

mod5 = glmer(chosen~pos_y+I(pos_y^2)+salience+sales+quality+familiarStore+familiarCat + sales:quality + (1|product) + (1|participant), df, family='binomial', nAGQ=0)
summary(mod5)
drop1(mod5, test='Chisq') # no effect of familiarity



### with eye-tracking data:
mod6 = glmer(chosen~pos_y+I(pos_y^2)+salience+sales*quality + dwelltime+redwells+noFacings + (1|product) + (1|participant), df, family='binomial', nAGQ=0) 
summary(mod6) # cf.summary(mod3)
drop1(mod6, test='Chisq')
s = summary(mod6)$coefficients
output = data.frame(pos_y=seq(min(df$pos_x, na.rm=T), max(df$pos_x, na.rm=T), length.out=100))
output$fit = antilogit(s[1,1] + s[2,1]*output$pos_y + s[3,1]*output$pos_y^2 + s[4,1]*median(df$salience) + s[5,1]*median(df$sales) + s[6,1]*median(df$quality) + s[7,1]*max(df$dwelltime) + s[8,1]*median(df$redwells) + s[9,1]*median(df$sales)*median(df$quality)) * 100
plot (output, ylab='Prob. of buying, %', xlab='Vertical position')


## cf. for only products with at least one fixation
df_looked = df[df$looked==T,]
mod7 = glmer(chosen~pos_y+I(pos_y^2)+salience+sales+quality + sales:quality + dwelltime+redwells + (1|product) + (1|participant), df_looked, family='binomial', nAGQ=0)
summary(mod7) # cf.summary(mod3)
drop1(mod7, test='Chisq')

# plot
# df$pos_y2 = df$pos_y^2
# mod_brm = brm(chosen~pos_y+I(pos_y^2)+salience+noFacings+sales*quality + dwelltime+redwells + (1|product) + (1|participant), data=df_looked, warmup=100, iter=500, chains=4, cores=4, family='bernoulli', ranef=F, prior=set_prior("normal(0,10)"))
# saveRDS(mod_brm, 'mod_chosen.RDS') 
# mod_brm = readRDS('mod_chosen.RDS') 
plot(mod_brm)
summary(mod_brm)

coda = posterior_samples(mod)
colnames(coda)

newdata=data.frame(pos_y=mean(df_looked$pos_y), Ipos_yE2=mean(df_looked$pos_y)^2,
                   salience=mean(df_looked$salience), sales=mean(df_looked$sales), 
                   noFacings=mean(df_looked$noFacings), 
                   quality=mean(df_looked$quality), redwells=mean(df_looked$redwells), 
                   dwelltime=seq(min(df_looked$dwelltime), max(df_looked$dwelltime), length.out=100))
newdata=cbind(newdata, fitted(mod_brm, newdata, re_formula = NA, scale='response')*100)
colnames(newdata)[(ncol(newdata)-3):ncol(newdata)] = c('fit','se','lwr','upr')
newdata$dwelltime = exp(newdata$dwelltime*log(52206)) / 1000

# add aggregated observed data
# specify the bins for overaging observed data
nBins_obs=100
s1 = seq(min(df_looked$dwelltime, na.rm=T), max(df_looked$dwelltime, na.rm=T), length.out=nBins_obs+1)  
# s1 = quantile(df_looked$dwelltime, probs=seq(0,1,length.out=nBins_obs+1)) # 

# matrix with bin averages
# out_obs = data.frame(dwelltime=s1, chosen=NA) 
out_obs = data.frame(dwelltime=s1+1/nBins_obs, chosen=NA)
for (i in 1:nBins_obs){
    out_obs$chosen[i] = mean(df_looked[df_looked$dwelltime>s1[i] & df_looked$dwelltime<s1[i+1], 'chosen']) *100
}
out_obs$dwelltime = exp(out_obs$dwelltime*log(52206))/1000

r = c( floor(log2(min(out_obs$dwelltime))), ceiling(log2(max(out_obs$dwelltime))) )
mybreaks = round(2^(seq(r[1],r[2],by=1)),2)

library(ggplot2)
library(scales)
ggplot (newdata, aes(x=dwelltime, y=fit)) +
  geom_line() +
  geom_ribbon(aes(ymin=lwr, ymax=upr), linetype='blank', alpha=0.2) +
  geom_point(data=out_obs, aes(x=dwelltime, y=chosen), inherit.aes = F) +
  scale_x_continuous(trans=log2_trans(), breaks=mybreaks) +
  xlab('Dwell time (s)') + ylab('Probability of buying (%)') +
  theme_bw() +
  theme(panel.grid = element_blank())

# ggsave('pix/fig4_dwell~choice.jpg', width=7, height=7, units='cm', dpi=300, scale=1.5)



###
# ********
###
# Prediction accuracy
library (randomForest)

mod1 = randomForest (as.factor(chosen)~pos_y+dwelltime+salience+sales+quality+noFacings+redwells+familiarStore+familiarCat, df, ntree=2000, strata=df$chosen, sampsize=rep(min(table(df$chosen)), 2))
plot (mod1)
mod1 # 94% correct
varImpPlot (mod1, class='1') # dwelltime > redwells > sales > quality > salience > everything else

mod3 = randomForest (as.factor(chosen)~pos_y, df, ntree=2000, strata=df$chosen, sampsize=rep(min(table(df$chosen)), 2))
mod3 # dwelltime 93% correct, quality 70%, salience 62%, sales 62%, noFacings 77%, redwells 88%, 


# bootstrap to get CIs for recognition rates
noIter = 200
ptm <- proc.time()  
vars = c('pos_y','salience','sales','noFacings','quality','dwelltime','redwells')
output = array (dim=c(2, length(vars), noIter))

for (v in 1:length(vars)){
  myvar = vars[v]
  for (i in 1:noIter) {
    model.boot = randomForest (as.factor(chosen)~df[,myvar], df, strata=df$chosen, sampsize=rep(min(table(df$chosen)), 2))
    output [,v,i] = 1-apply (model.boot$err.rate, 2, mean)[2:3] 
    time_elapsed = as.numeric ((proc.time() - ptm)[3])
    time_left = time_elapsed/i * (noIter-i)
    minutes = time_left %/% 60; seconds = round (time_left %% 60, 0)
    if (minutes>0) {
      report = paste0('Done with iteration ',i, ' of ', noIter, '. Time left: ', minutes, ' min ', seconds, ' sec.')
    } else {
      report = paste0('Done with iteration ',i, ' of ', noIter, '. Time left: ', seconds, ' sec.')
    }
    print (report)
  }
}

# result = round ( apply (output, c(1,2), mean)*100, 0)
result = apply (output, c(1,2), function(x){
  paste0(round(mean(x)*100, 0), ' [', round(quantile(x, probs=.025)*100, 0), ', ', round(quantile(x, probs=.975)*100, 0), ']')
})
colnames(result) = vars
rownames(result) = c('TN','TP')
result = t(result)
# write.csv (result, '~/Downloads/temp.csv')


# illustration of the effect of noFacings
mod4 = randomForest (as.factor(chosen)~noFacings, df, ntree=2000, strata=df$chosen, sampsize=rep(min(table(df$chosen)), 2))
mod4
a = data.frame(noFacings=seq(min(df$noFacings),max(df$noFacings),length.out=100))
a$chosen = predict(mod4, a, type='prob')[,2]
plot(1:nrow(a),a$chosen, type='l')



