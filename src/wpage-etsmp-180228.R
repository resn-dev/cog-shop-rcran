#~ ##################################################################### 180212
#~ resume after image trial start triggers
mdf = read.table('~/dev/python/opencv/cog-shop/csv/dataset.csv', sep='\t', header=T, strip.white=TRUE)

#~ explore dependent variables
mdf$aoi_choosen
with(mdf, hist(log(et_samples_count)))
#~ explore predictors
with(mdf, hist(aoi_saliency_mean))
with(mdf, hist(sqrt(pos2)))
with(mdf, hist(OQ))

#~ clean up
mdf$trial_id_task = as.factor(mdf$trial_id_task)
mdf$aoi_saliency_mean = as.numeric(mdf$aoi_saliency_mean)
mdf$aoi_saliency_sum = as.numeric(mdf$aoi_saliency_sum)
#~ text to columns
#~ head(strsplit(mdf$aoi_position_xywh, '\\|'))
library(stringr)
head(str_split_fixed(mdf$aoi_position_xywh, '\\|', 4))
#~ install.packages("data.table", lib="~/lib/r-cran")
library(data.table)
setDT(mdf)[, paste0("pos", 1:4) := tstrsplit(aoi_position_xywh, "|", type.convert=TRUE, fixed=TRUE)]
#~ convert sample count to binary looked (12 samples = 100 msec)
mdf$et_looked_bin = ifelse(mdf$ac=="png",0,1)

#~ diagnostics
with(mdf, hist(et_missing_prop, 100))
#~ plot proportion looked against y values
plot(tapply(mdf$et_looked_binary, mdf$pos2, mean))

with(subset(mdf, et_verify_binary==0), table(paste(trial_id_task, aoi_id_product), et_verify_binary))

length(which(mdf$et_verify_binary==1))

length(which(df$bought_produkt==1 & df$aoi_visible_binary==0))
length(which(df$bought_produkt==1 & df$et_looked_binary==0))
length(which(df$bought_produkt==1 & df$et_samples_count==0))

#~ ##################################################################### 180124
#~ resume from joost 
df = read.table("tmp/AllDataWeb180105.csv", header=T, sep="\t", stringsAsFactors=F)
str(df)
#~ 8536 obs. of  22 variables
plot(tapply(df$et_looked_binary, df$y, mean))

normalize <- function(x) {return ((x - min(x)) / (max(x) - min(x)))}

#~ check for anomalies
#~ length(which(df$bought_produkt==1)), length(which(df$et_verify_binary==1))

length(which(df$bought_produkt==1 & df$aoi_visible_binary==0))
length(which(df$bought_produkt==1 & df$et_looked_binary==0))
length(which(df$bought_produkt==1 & df$et_samples_count==0))

#~ get row by index
df[3919,]


#~ create exclusion variable
subset(data, D1 == "E" | D2 == "E")

#~ ##################################################################### 180105
#~ read dataset
library(readxl)
mdf = read_excel("tmp/AllDataWeb180105.xlsx", sheet="clean")#na="NA"
#~ clean up
mdf$trial_id_task = as.factor(mdf$trial_id_task)
mdf$aoi_saliency_mean = as.numeric(mdf$aoi_saliency_mean)
mdf$aoi_saliency_sum = as.numeric(mdf$aoi_saliency_sum)
mdf$et_missing_prop = as.numeric(mdf$et_missing_prop)


#~ explore dependent variables
mdf$aoi_choosen
with(mdf, hist(log(et_samples_count)))
#~ explore predictors
with(mdf, hist(aoi_saliency_mean))
with(mdf, hist(sqrt(pos2)))
with(mdf, hist(OQ))


library(lme4)




#~ ##################################################################### 171214
#~ read dataset
mdf = read.table('~/dev/python/opencv/cog-shop/csv/dataset.csv', sep='\t', header=T, strip.white=TRUE)
#~ try readxl
install.packages("devtools", lib="~/lib/r-cran")
devtools::install_github("hadley/readxl")
library(devtools)
with_libpaths(new='~/lib/r-cran/', install_github('hadley/readxl'))
library(readxl)
mdf = read_excel("tmp/AllDataWeb.xlsx", sheet="all")#na="NA"
#~ clean up
mdf$trial_id_task = as.factor(mdf$trial_id_task)
mdf$aoi_saliency_mean = as.numeric(mdf$aoi_saliency_mean)
mdf$aoi_saliency_sum = as.numeric(mdf$aoi_saliency_sum)
#~ text to columns
head(strsplit(mdf$aoi_position_xywh, '\\|'))
library(stringr)
head(str_split_fixed(mdf$aoi_position_xywh, '\\|', 4))
#~ install.packages("data.table", lib="~/lib/r-cran")
setDT(mdf)[, paste0("pos", 1:4) := tstrsplit(aoi_position_xywh, "|", type.convert=TRUE, fixed=TRUE)]
#~ convert sample count to binary looked (12 samples = 100 msec)
mdf$et_looked_bin = ifelse(mdf$ac=="png",0,1)

#~ #####################################################################
#~ explore dependent variables
mdf$aoi_choosen
with(mdf, hist(log(et_samples_count)))
#~ explore predictors
with(mdf, hist(aoi_saliency_mean))
with(mdf, hist(sqrt(pos2)))
with(mdf, hist(OQ))








