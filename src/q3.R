## Question 3: what predicts the optimality of the final choice (e.g. the quality of the chosen product)?

setwd ('/home/allgoodguys/Documents/Studying/Lund_PhD/z_helping-out/Kerstin_gidlof/03_june')
library (lme4)
library (brms)

orig = read.csv ('all.csv')
df = read.csv ('all_preprocessed_coor.csv')
df = df[,c('chosen','pos_y','salience','quality','sales','noFacings','dwelltime','redwells','familiarStore','familiarCat','familiarProduct','product','participant')]
mean (df$quality, na.rm=T); sd(df$quality, na.rm=T)

length(unique(df$participant))
length(unique(df$product))

# df = na.omit (df[df$chosen==T,])
df = df[df$chosen==T,]
mean (df$quality, na.rm=T); sd(df$quality, na.rm=T)
hist (df$quality)

mod1 = lm(quality~pos_y+I(pos_y^2)+dwelltime+redwells+sales+salience+noFacings+redwells+familiarStore+familiarCat+familiarProduct, df)
summary(mod1) # only one really strong, NEGATIVE predictor: sales. Familiarity with category is marginally negative as well, everything else ~0
drop1(mod1, test='F')
cor(df$sales, df$familiarProduct)

mod2 = lm(quality~sales+familiarCat+salience+familiarProduct, df)
summary(mod2) 
drop1(mod2, test='F')
