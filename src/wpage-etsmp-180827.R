#~ ##################################################################### 180817
#study b
library(lme4)
#~ depvar: et samples (aoi_visible_binary = missing data)
#m = lmer(et_samples_count ~ sa1_mean_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=sa1, subset=et_samples_count<1000 & aoi_visible_binary==1)
m = lmer(et_samples_count ~ sa4_sum_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=sa4, subset=et_samples_count<1000 & aoi_visible_binary==1)
#simple linear model
m = glm(et_samples_count ~ sa4_sum_norm + aoi_vpos_norm, data=sa4, subset=et_samples_count<1000 & aoi_visible_binary==1)

#~ ##################################################################### 180817
#study a

###################### reconstruct original model
adf = read.table('csv/all.csv', sep=',', header=T, strip.white=TRUE)
adf = read.table('csv/all_preprocessed_coor.csv', sep=',', header=T, strip.white=TRUE)

#get aoi id
adf$aoi_category = tolower(substring(adf$cat, 1, 1))
adf$aoi_store = tolower(substring(adf$store, 1, 1))
adf$aoi_id = paste(adf$aoi_store, adf$aoi_category, tolower(adf$aoi), sep='-')
#Factor w/ 438 levels "m-c-a1","m-c-a10"

###################### check that coefficients match
library(lme4)
# use neg. binomial because the overdispersion is monstrous (deviance/df.resid >> 1)
mod0 = glmer(looked~salience+sales+quality+familiarStore+familiarCat + (1|product) + (1|participant), data=adf, family='binomial', nAGQ=0)
summary(mod0)

#descriptives
hist(adf$salience)

###################### swap saliency variables
sa5 = read.table('~/dev/python/opencv/cog-shop/csv/study-a/aoi-saliency.csv', sep='\t', header=T, strip.white=TRUE)

#~ get aoi id, merge data
#Factor w/ 441 levels "m-c-a1","m-c-a10"
sa5 = merge(adf, sa5, by='aoi_id')

#normalize values
sa5$pysal_mean[is.na(sa5$pysal_mean)==T] = 0
sa5$pysal_mean_norm = normalize(sa5$pysal_mean)
sa5$pysal_sum[is.na(sa5$pysal_sum)==T] = 0
sa5$pysal_sum_norm = normalize(sa5$pysal_sum)
#?
sa5$aoiy[is.na(sa5$aoiy)==T] = 0
sa5$aoi_vpos_norm = normalize(sa5$aoiy)

#compare
plot(sa5$salience, type="l", col="red")
lines(sa5$pysal_mean_norm, col="green")
lines(sa5$pysal_sum_norm, col="blue")
#descriptives
h1 = hist(sa5$salience); h2 = hist(sa5$pysal_mean_norm)
plot(h1, col=rgb(0,0,1,1/4), xlim=c(0,1), ylim=c(0,3000))
plot(h2, col=rgb(1,0,0,1/4), xlim=c(0,1), ylim=c(0,3000), add=T)
#?
x = aggregate(list(mlsal=sa5$salience, pysal=sa5$pysal_mean_norm), by=list(sa5$aoi_id), FUN=mean, na.rm=TRUE)
plot(x$mlsal, type="l", col="red")
lines(x$pysal, col="green")
#
write.table(x, "~/dev/python/opencv/cog-shop/csv/study-a/comp-saliency.csv", sep="\t", row.names=F)


###################### check if model is different
library(lme4)
mod1 = glmer(looked~pysal_mean_norm+sales+quality+familiarStore+familiarCat + (1|product) + (1|participant), data=sa5, family='binomial', nAGQ=0)
summary(mod1)

#~ ##################################################################### 180618
#~ python implementation of saliency analysis, study a
sa5 = read.table('~/dev/python/opencv/cog-shop/csv/study-a/aoi-saliency.csv', sep='\t', header=T, strip.white=TRUE)

#get aoi id
ydf$aoi_category = tolower(substring(ydf$cat, 1, 1))
ydf$aoi_store = tolower(substring(ydf$store, 1, 1))
ydf$aoi_id = paste(ydf$aoi_store, ydf$aoi_category, tolower(ydf$aoi), sep='-')

#~ merge data
sa5 = merge(ydf, sa5, by='aoi_id')

#normalize values
sa5$pysal_mean[is.na(sa5$pysal_mean)==T] = 0
sa5$pysal_mean_norm = normalize(sa5$pysal_mean)
sa5$pysal_sum[is.na(sa5$pysal_sum)==T] = 0
sa5$pysal_sum_norm = normalize(sa5$pysal_sum)
sa5$aoiy[is.na(sa5$aoiy)==T] = 0
sa5$aoi_vpos_norm = normalize(sa5$aoiy)

#reconstruct original model
#check that coefficients match
#swap saliency variables
#check if model is different

library(lme4)
#m = lmer(et_samples_count ~ sa4_sum_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=sa4, subset=et_samples_count<1000 & aoi_visible_binary==1)
m = lmer(dwelltime ~ pysal_mean_norm + quality + sales + aoi_vpos_norm + (1|aoi_id) + (1|participant), data=sa5)
summary(m)

#~ ##################################################################### 180613
#~ decision quality, option quality on chosen
dqdf1 = ydf[ydf$chosen==T, c("study_id","store","cat","participant","chosen","quality")]
#same categories
dqdf1$cat = gsub("Cereal", "c", dqdf1$cat)
dqdf1$cat = gsub("Pasta", "p", dqdf1$cat)
dqdf1$cat = gsub("Yog", "y", dqdf1$cat)
dqdf1$cat = as.factor(dqdf1$cat)

#new data
dqdf2 = sa3[sa3$bought_produkt==1, c("study_id","col","trial_id_task","subject_id","bought_produkt","OQ")]
#same col names
names(dqdf2) = c("study_id","store","cat","participant","chosen","quality")

#append studies 
dqdf = rbind(dqdf1, dqdf2)

#compare means 
aggregate(quality ~ study_id + cat, data=dqdf, FUN=mean)

#t-test between physical and digital 
t.test(dqdf1$quality,dqdf2$quality)
#for each category
sapply(split(dqdf, dqdf$cat), function(x) t.test(x$quality[x$study_id=="a"], x$quality[x$study_id=="b"]))


#~ ##################################################################### 180611
#~ alternative saliency matlab, aoi relative to frame
sa4 = read.table('csv/saliency-alt4.csv', sep='\t', header=T, strip.white=TRUE)
#~ merge data
sa4 = merge(xdf, sa4, by='aoi_id')
#~ normalize summed saliency
sa4$sa4_sum_norm = normalize(sa4$salt4_sum)
sa4$sa4_mean_norm = normalize(sa4$salt4_mean)
#convert trial factor levels to numeric
sa4$col = as.numeric(as.factor(sa4$trial_id_task))

library(lme4)
#~ depvar: et samples (aoi_visible_binary = missing data)
#m = lmer(et_samples_count ~ sa1_mean_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=sa1, subset=et_samples_count<1000 & aoi_visible_binary==1)
m = lmer(et_samples_count ~ sa4_sum_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=sa4, subset=et_samples_count<1000 & aoi_visible_binary==1)
#simple linear model
m = glm(et_samples_count ~ sa4_sum_norm + aoi_vpos_norm, data=sa4, subset=et_samples_count<1000 & aoi_visible_binary==1)


#~ ##################################################################### 180609
#~ add subject shopping variables
sdf = read.table('csv/subject-shopping-variables.tsv', sep='\t', header=T, strip.white=TRUE)

#~ ##################################################################### 180608
#~ append dataset from physical store
ydf = read.table('csv/all_preprocessed_coor.csv', sep=',', header=T, strip.white=TRUE)

#provide row id, relevant columns (DVs, IVs, RFs, Cs)
ydf$row_id = seq.int(nrow(ydf))
sa3$row_id = seq.int(nrow(sa3))
#create column for study
ydf$study_id = as.factor("a")
sa3$study_id = as.factor("b")
#get column numbers
as.matrix(names(ydf))
as.matrix(names(sa3))

###################### select and order dependent variables
dvdf1 = ydf[ , c(27,10,18,15,16,28)]
dvdf2 = sa3[ , c(38,33,12,13,20,39)]
#define column names
cn = c(
"row_id",
"et_dwell_norm","et_looked_bin","et_revisits_norm",
"aoi_chosen_bin",
"study_id")
#rename columns
names(dvdf1) = cn
names(dvdf2) = cn
#harmonize data types and transforms (normalization, centering, etc)
dvdf1[, c("et_looked_bin","aoi_chosen_bin")] = as.numeric(unlist(dvdf1[, c("et_looked_bin","aoi_chosen_bin")]))
dvdf2$et_revisits_norm = normalize(dvdf2$et_revisits_norm)
#append rows
dvdf = rbind(dvdf1, dvdf2)

###################### select and order independent variables
ivdf1 = ydf[ , c(27,11,11,1,1,1,21,21,1,1,1,28)]
ivdf2 = sa3[ , c(38,35,36,1,1,1,24,34,1,1,1,39)]
#define column names
cn = c(
"row_id",
"aoi_saliency_sum","aoi_saliency_norm","aoi_sa0","aoi_sa1","aoi_sa2",
"aoi_vpos_abs","aoi_vpos_norm","aoi_vpos0","aoi_vpos1","aoi_vpos2",
"study_id")
#rename columns
names(ivdf1) = cn
names(ivdf2) = cn
#harmonize data types and transforms (normalization, centralization, etc)
#
#append rows
ivdf = rbind(ivdf1, ivdf2)

###################### select and order random factors and controls
rcdf1 = ydf[ , c(27,19,4,3,28)]
rcdf2 = sa3[ , c(38,1,3,4,39)]
#define column names
cn = c(
"row_id",
"aoi_id","aoi_category",
"subject_id",
"study_id")
#rename columns
names(rcdf1) = cn
names(rcdf2) = cn
#harmonize data types and transforms (normalization, centralization, etc)
rcdf1[, c("subject_id")] = as.numeric(unlist(rcdf1[, c("subject_id")]))
rcdf1$aoi_category = gsub("Cereal", "c", rcdf1$aoi_category)
rcdf1$aoi_category = gsub("Pasta", "p", rcdf1$aoi_category)
rcdf1$aoi_category = gsub("Yog", "y", rcdf1$aoi_category)
rcdf1$aoi_category = as.factor(rcdf1$aoi_category)
#
rcdf2$aoi_id = as.factor(rcdf2$aoi_id)
rcdf2$aoi_category = as.factor(rcdf2$aoi_category)
#append rows
rcdf = rbind(rcdf1, rcdf2)

#merge dv, iv, rc columns
require(sqldf)
#df = sqldf("select dv.*, iv.* from dvdf dv, ivdf iv where dv.row_id = iv.row_id and dv.study_id = iv.study_id", drv='SQLite')
#df = merge(dvdf, ivdf, by='row_id')
df = sqldf("select dv.*, iv.*, rc.* from dvdf as dv 
inner join ivdf as iv on iv.row_id = dv.row_id and iv.study_id = dv.study_id 
inner join rcdf as rc on rc.row_id = dv.row_id and rc.study_id = dv.study_id", drv='SQLite')

#test model
library(lme4)
#m = lmer(et_samples_norm ~ sa3_sum_norm + aoi_vpos_visible + (1|subject_id) + (1|aoi_id), data=sa3, subset=aoi_visible_binary==1)
m = lmer(et_dwell_norm ~ aoi_saliency_norm + aoi_vpos_norm + (1|subject_id) + (1|aoi_id), data=df, subset=study_id=="b")



#~ ##################################################################### 180604
#~ aoi vertical position, normalize against individual max scroll (aoi visible) 

#explore
#install.packages("sqldf", lib="~/lib/r-cran")
require(sqldf)
sqldf("select a.trial_id_task, a.subject_id, max(a.pos2) as yval from xdf a where a.subject_id in (11,13,15) and a.aoi_visible_binary=1 group by a.trial_id_task, a.subject_id", drv='SQLite')

#create new variable
xdf$aoi_vpos_visible = NA

#get number of rows for single participant
nrow(xdf[(xdf$trial_id_task=="c" & xdf$subject_id==15), c("subject_id","pos2","aoi_visible_binary")])
#[1] 47

#replace vpos values locally
xdf$aoi_vpos_visible[(xdf$trial_id_task=="c" & xdf$subject_id==15 & xdf$aoi_visible_binary==1)] = normalize(xdf[(xdf$trial_id_task=="c" & xdf$subject_id==15 & xdf$aoi_visible_binary==1), c("pos2")])

#check number of replaced values (non-visible rows still NA)
nrow(xdf[!is.na(xdf$aoi_vpos_visible), c("subject_id","pos2","aoi_visible_binary")])
#[1] 44

#check by row range
xdf[517:565, c("subject_id","pos2","aoi_visible_binary","aoi_vpos_visible")]

#iterate over df, calculate normalized aoi vertical position
for (i in unique(xdf$subject_id)) { print(i); for (j in unique(xdf$trial_id_task[xdf$subject_id==i])) { print(paste(i,j)); xdf$aoi_vpos_visible[(xdf$trial_id_task==j & xdf$subject_id==i & xdf$aoi_visible_binary==1)] = normalize(xdf[(xdf$trial_id_task==j & xdf$subject_id==i & xdf$aoi_visible_binary==1), c("pos2")]) } }

#how many rows not visible
nrow(xdf[is.na(xdf$aoi_vpos_visible),])

#effect of new vpos measure
library(lme4)
m = lmer(et_samples_count ~ aoi_salsum_norm * OQ + Sales_online + aoi_vpos_visible + (1|subject) + (1|product), data=xdf, subset=et_samples_count<1000 & aoi_visible_binary==1)

#~ ##################################################################### 180514
#~ alternative saliency matlab, aoi relative to wpage
sa3 = read.table('csv/saliency-alt3.csv', sep='\t', header=T, strip.white=TRUE)
#~ merge data
sa3 = merge(xdf, sa3, by='aoi_id')
#~ normalize summed saliency
sa3$sa3_sum_norm = normalize(sa3$salt3_sum)
#convert trial factor levels to numeric
sa3$col = as.numeric(as.factor(sa3$trial_id_task))


#~ visualize: et samples depending on saliency and trial (color)
png('fig/correlation-5.png', width=500, height=500)
# all, top frame
tmp = aggregate(et_samples_count ~ sa3_sum_norm + col, data=subset(sa3, et_samples_count<1000), mean)
colnames(tmp) = c('aoisal','col','etsmp')
plot(etsmp~aoisal, col='blue', xlab='saliency', ylab='samples', data=tmp)
lfit = loess(etsmp~aoisal, tmp)
#lines(tmp$aoisal, predict(lfit), col='blue')
abline(lm(etsmp~aoisal, tmp), col='blue')
# 1 black, cereal
tmp = aggregate(et_samples_count ~ sa3_sum_norm + col, data=subset(sa3, et_samples_count<1000 & col==1), mean)
colnames(tmp) = c('aoisal','col','etsmp')
points(etsmp~aoisal, col=1, data=tmp)
lfit = loess(etsmp~aoisal, data=tmp)
abline(lm(etsmp~aoisal, data=tmp), col=1)
# 2 red, pasta
tmp = aggregate(et_samples_count ~ sa3_sum_norm + col, data=subset(sa3, et_samples_count<1000 & col==2), mean)
colnames(tmp) = c('aoisal','col','etsmp')
points(etsmp~aoisal, col=2, data=tmp)
lfit = loess(etsmp~aoisal, data=tmp)
abline(lm(etsmp~aoisal, data=tmp), col=2)
# 3 green, yoghurt
tmp = aggregate(et_samples_count ~ sa3_sum_norm + col, data=subset(sa3, et_samples_count<1000 & col==3), mean)
colnames(tmp) = c('aoisal','col','etsmp')
points(etsmp~aoisal, col=3, data=tmp)
lfit = loess(etsmp~aoisal, data=tmp)
abline(lm(etsmp~aoisal, data=tmp), col=3)
#
legend('topright', legend=c('c','p','y'), fill=c('black','red','green'))
dev.off()


library(lme4)
#~ depvar: et samples (aoi_visible_binary = missing data)
#m = lmer(et_samples_count ~ sa1_mean_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=sa1, subset=et_samples_count<1000 & aoi_visible_binary==1)
m = lmer(et_samples_count ~ sa3_sum_norm * OQ + Sales_online + aoi_vpos_norm + (1|col) + (1|subject) + (1|product), data=sa3, subset=et_samples_count<1000 & aoi_visible_binary==1)
#simple linear model
m = glm(et_samples_count ~ sa3_sum_norm + aoi_vpos_norm, data=sa3, subset=et_samples_count<1000 & aoi_visible_binary==1)


#run bash commands
system("ls -l fig/")
system("geany ~/cmd/r-cran.txt")

#~ ##################################################################### 180514
#~ alternative saliency, aoi relative to frame
sa2 = read.table('csv/saliency-alt2.csv', sep='\t', header=T, strip.white=TRUE)
#~ create merge variable
#xdf$aoi_id = paste0(paste(sprintf("%03d", xdf$aoi_id_product), xdf$trial_id_task, sep='-'), '.png')
#~ merge data
sa2 = merge(xdf, sa2, by='aoi_id')
#~ normalize saliency
sa2$sa2_sum_norm = normalize(sa2$salt2_sum)
sa2$sa2_mean_norm = normalize(sa2$salt2_mean)
#convert trial factor levels to numeric
sa2$col = as.numeric(as.factor(sa2$trial_id_task))


#~ visualize: et samples depending on saliency and trial (color)
png('fig/correlation-4.png', width=500, height=500)
# all, top frame
tmp = aggregate(et_samples_count ~ sa2_mean_norm + col, data=subset(sa2, et_samples_count<1000 & pos2 < 1050), mean)
colnames(tmp) = c('aoisal','col','etsmp')
plot(etsmp~aoisal, col=0, xlab='saliency', ylab='samples', data=tmp)
lfit = loess(etsmp~aoisal, tmp)
#lines(tmp$aoisal, predict(lfit), col='blue')
abline(lm(etsmp~aoisal, tmp), col='blue')
# 1 black, cereal
tmp = aggregate(et_samples_count ~ sa2_mean_norm + col, data=subset(sa2, et_samples_count<1000 & col==1 & pos2 < 1050), mean)
colnames(tmp) = c('aoisal','col','etsmp')
points(etsmp~aoisal, col=1, data=tmp)
lfit = loess(etsmp~aoisal, data=tmp)
abline(lm(etsmp~aoisal, data=tmp), col=1)
# 2 red, pasta
tmp = aggregate(et_samples_count ~ sa2_mean_norm + col, data=subset(sa2, et_samples_count<1000 & col==2 & pos2 < 1050), mean)
colnames(tmp) = c('aoisal','col','etsmp')
points(etsmp~aoisal, col=2, data=tmp)
lfit = loess(etsmp~aoisal, data=tmp)
abline(lm(etsmp~aoisal, data=tmp), col=2)
# 3 green, yoghurt
tmp = aggregate(et_samples_count ~ sa2_mean_norm + col, data=subset(sa2, et_samples_count<1000 & col==3 & pos2 < 1050), mean)
colnames(tmp) = c('aoisal','col','etsmp')
points(etsmp~aoisal, col=3, data=tmp)
lfit = loess(etsmp~aoisal, data=tmp)
abline(lm(etsmp~aoisal, data=tmp), col=3)
#
legend('topright', legend=c('c','p','y'), fill=c('black','red','green'))
dev.off()

#only pasta
sa3 = subset(sa2, et_samples_count<1000 & pos2<1050 & col==2)
plot(et_samples_count~sa2_mean_norm, xlab='saliency', ylab='samples', data=sa3)
lfit = loess(et_samples_count~sa2_mean_norm, sa3)
#lines(tmp$aoisal, predict(lfit), col='blue')
abline(lm(et_samples_count~sa2_mean_norm, sa3))


library(lme4)
#~ depvar: et samples (aoi_visible_binary = missing data)
#m = lmer(et_samples_count ~ sa1_mean_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=sa1, subset=et_samples_count<1000 & aoi_visible_binary==1)
m = lmer(et_samples_count ~ sa2_mean_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=sa2, subset=et_samples_count<1000 & aoi_visible_binary==1)
m = lmer(et_samples_count ~ sa2_mean_norm * OQ + Sales_online + aoi_vpos_norm + (1|col) + (1|subject) + (1|product), data=sa2, subset=et_samples_count<1000 & aoi_visible_binary==1)


#~ ##################################################################### 180508
#~ alternative saliency, absolute aoi value
sa1 = read.table('csv/saliency-alt1.csv', sep='\t', header=T, strip.white=TRUE)
#~ create merge variable
xdf$aoi_id = paste0(paste(sprintf("%03d", xdf$aoi_id_product), xdf$trial_id_task, sep='-'), '.png')
#~ merge data
sa1 = merge(xdf, sa1, by='aoi_id')
#~ normalize saliency
sa1$sa1_sum_norm = normalize(sa1$salt1_sum)
sa1$sa1_mean_norm = normalize(sa1$salt1_mean)


#~ visualize: et samples depending on saliency
tmp = aggregate(et_samples_count ~ sa1_mean_norm, data=subset(sa1, et_samples_count<1000), mean)
colnames(tmp) = c('aoisal','etsmp')
plot(tmp)
lfit = loess(etsmp~aoisal, tmp)
lines(tmp$aoisal, predict(lfit), col='red')
abline(lm(etsmp~aoisal, tmp), col='blue')


library(lme4)
#~ depvar: et samples (aoi_visible_binary = missing data)
m = lmer(et_samples_count ~ sa1_mean_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=sa1, subset=et_samples_count<1000 & aoi_visible_binary==1)


#~ ##################################################################### 180418
#~ get some descriptive stats

#~ visualize: et samples depending on saliency
tmp = aggregate(et_samples_count ~ aoi_salsum_norm, data=subset(xdf, et_samples_count<1000), mean)
colnames(tmp) = c('aoisal','etsmp')
plot(tmp)
lfit = loess(etsmp~aoisal, tmp)
lines(tmp$aoisal, predict(lfit), col='red')
abline(lm(etsmp~aoisal, tmp), col='blue')


#~ ##################################################################### 180328
#~ get some descriptive stats for lucs seminar
#~ - avg time per product aoi, and category
#~ - avg time per category, trial interval
#~ - avg number of aoi per category
#~ - choice quality
#~ - pupil size
#~ - enter time into regression model
#~ - time as factor in the model
#~ - less anti-saliency over time?

#~ 1. summarize data
library(plyr)
cdf <- ddply(xdf, c("trial_id_task"), summarise,
N=length(et_samples_count), mean=mean(et_samples_count), sd=sd(et_samples_count), se=sd/sqrt(N))
#~ > cdf
#~   trial_id_task    N      mean       sd       se
#~ 1             c 2021 119.03958 182.3631 4.056522
#~ 2             p 5719  50.23763 106.1568 1.403744
#~ 3             y 2520 101.04921 196.1829 3.908057

#~ 2. bar plot data 
bary = cdf$mean
bare = cdf$se
#~ to file
png('fig/descriptives-1.png', width=500, height=500)
#~ set margins
#~ par(mar=c(5.1, 5.1, 4.1, 4.1))
#~ bar plot
barx = barplot(bary, space=c(1,1), names.arg=c("c","p","y"), xlab="product category", ylim=c(0,150), ylab="mean et samples", axis.lty=1,
cex.names=1.5, cex.axis=1.5, cex.lab=1.5, col=c("gray50"), main="mean aoi et samples by product category", cex.main=1.5)
arrows(barx, bary+bare, barx, bary-bare, angle=90, code=3, length=0.1)
#~ legend("topleft", legend=c("Comprehension, children","Comprehension, adults"), fill=c("red","gray50"), bty="n", cex=1.5)
#~ box()
dev.off()

#~ cat csv/video/coding-images-* | egrep -v '\s0\s' > tmp/coding-images.csv
#~ read trial intervals
tdf = read.table('~/dev/python/opencv/cog-shop/tmp/coding-images.csv', sep='\t', header=T, strip.white=TRUE)
#~ get trial duration
tdf$td = (tdf$c - tdf$s) / 120.0
#~ aggregate
cdf <- ddply(tdf, c("t"), summarise,
N=length(td), mean=mean(td), sd=sd(td), se=sd/sqrt(N))
#~ 2. bar plot data 
bary = cdf$mean
bare = cdf$se
#~ to file
png('fig/descriptives-2.png', width=500, height=500)
#~ bar plot
barx = barplot(bary, space=c(1,1), names.arg=c("c","p","y"), xlab="product category", ylim=c(0,60), ylab="seconds", axis.lty=1,
cex.names=1.5, cex.axis=1.5, cex.lab=1.5, col=c("gray50"), main="mean trial duration by product category", cex.main=1.5)
arrows(barx, bary+bare, barx, bary-bare, angle=90, code=3, length=0.1)
#~ 
dev.off()


tmp = aggregate(aoi_visible_binary ~ subject + trial_id_task, data=xdf, sum)
cdf <- ddply(tmp, c("trial_id_task"), summarise,
N=length(aoi_visible_binary), mean=mean(aoi_visible_binary), sd=sd(aoi_visible_binary), se=sd/sqrt(N))
#~ 2. bar plot data 
bary = cdf$mean
bare = cdf$se
#~ to file
png('fig/descriptives-3.png', width=500, height=500)
#~ bar plot
barx = barplot(bary, space=c(1,1), names.arg=c("c","p","y"), xlab="product category", ylim=c(0,100), ylab="mean number of visible aois", axis.lty=1,
cex.names=1.5, cex.axis=1.5, cex.lab=1.5, col=c("gray50"), main="mean number of visible aois by product category", cex.main=1.5)
arrows(barx, bary+bare, barx, bary-bare, angle=90, code=3, length=0.1)
#~ 
dev.off()


tmp = aggregate(et_looked_binary ~ subject + trial_id_task, data=xdf, sum)
cdf <- ddply(tmp, c("trial_id_task"), summarise,
N=length(et_looked_binary), mean=mean(et_looked_binary), sd=sd(et_looked_binary), se=sd/sqrt(N))
#~ 2. bar plot data 
bary = cdf$mean
bare = cdf$se
#~ to file
png('fig/descriptives-4.png', width=500, height=500)
#~ bar plot
barx = barplot(bary, space=c(1,1), names.arg=c("c","p","y"), xlab="product category", ylim=c(0,80), ylab="mean number of viewed aois", axis.lty=1,
cex.names=1.5, cex.axis=1.5, cex.lab=1.5, col=c("gray50"), main="mean number of viewed aois by product category", cex.main=1.5)
arrows(barx, bary+bare, barx, bary-bare, angle=90, code=3, length=0.1)
#~ 
dev.off()


#~ ##################################################################### 180328
#~ continue with complete excel dataset (combined et, saliency, and preferences)

#~ manual check saliency
#~ geeqie img/samples/

#~ == dependent variables
#~ check for outliers
#~ et_samples_count, convert to msec or sec
#~ et_looked_binary
#~ bought_produkt, binary

#~ == predictors
#~ norm vertical position
#~ saliency mean, sum, normalize
xdf$aoi_salsum_norm = normalize(xdf$aoi_saliency_sum)
#~ Sales_online (cf. sales_offline)
#~ OQ, option quality
xdf$aoi_saliency_mean = as.numeric(xdf$aoi_saliency_mean)
xdf$aoi_saliency_sum = as.numeric(xdf$aoi_saliency_sum)
xdf$aoi_vpos_norm = xdf$"Norm vertical position"
#~ check correlation predictors
with(xdf, pairs(list(aoi_saliency_mean, Sales_online, OQ), gap=0, lower.panel=panel.smooth, upper.panel=panel.cor))
pairs(xdf[,c("et_samples_count","aoi_salsum_norm","OQ","Sales_online","aoi_vpos_norm")], pch=19, cex=0.5)

#~ == random factors
#~ subject, product aoi
xdf$subject = as.factor(xdf$subject_id)
xdf$product = paste(xdf$trial_id_task, xdf$aoi_id_product, sep='_')
xdf$product = as.factor(xdf$product)

library(lme4)
#~ depvar: et samples (aoi_visible_binary = missing data)
m = lmer(et_samples_count ~ aoi_saliency_mean + OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=xdf, subset=et_samples_count<500)
m = lmer(et_samples_count ~ aoi_salsum_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=xdf, subset=et_samples_count<1000 & aoi_visible_binary==1)
#~ depvar: et looked binary
m = glmer(et_looked_binary ~ aoi_saliency_mean + OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=xdf, subset=et_samples_count<500, family="binomial")
m = glmer(et_looked_binary ~ aoi_salsum_norm * OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=xdf, subset=et_samples_count<1000 & aoi_visible_binary==1, family="binomial")
#~ depvar: aoi product chosen
xdf$et_samples_norm = normalize(xdf$et_samples_count)
m = glmer(bought_produkt ~ aoi_salsum_norm + OQ + Sales_online + aoi_vpos_norm + et_samples_norm + (1|subject) + (1|product), data=xdf, subset=et_samples_count<1000 & aoi_visible_binary==1, family="binomial")

#~ export dataset for joost
write.table(xdf, "csv/dataset-180328.csv", sep="\t")

#~ ##################################################################### 180309
#~ check dataset, offset horizontally and vertically
with(xdf, merge(et_samples_count,  et_offset_h=lag(et_samples_count,2), et_offset_v=lag(et_samples_count,-4)))
xdf$et_offset_h = c(xdf$et_samples_count[-1], NA)
xdf$et_offset_v = c(NA,NA,NA,NA, head(xdf$et_samples_count, -4))
#~ add size variable

#~ re-calculate samples into seconds
mdf$et_dwell_sec = as.numeric(mdf$et_samples_count)/120
mdf$et_dwell_msec = as.numeric(mdf$et_samples_count)/120*1000

#~ normalize predictors
mdf$aoi_posv_norm = normalize(mdf$pos2)
mdf$aoi_salmean_norm = normalize(mdf$aoi_saliency_mean)
mdf$aoi_salsum_norm = normalize(mdf$aoi_saliency_sum)

#~ visualize: mean samples over vertical position
tmp = aggregate(mdf$et_samples_count, list(mdf$pos2), mean)
tmp = aggregate(et_looked_binary~pos2, data=mdf, mean)
tmp = aggregate(et_samples_count~pos2, data=subset(mdf, et_samples_count<1000), mean)
tmp = aggregate(et_dwell_msec~aoi_posv_norm, data=subset(mdf, et_samples_count<1000), mean)
tmp = aggregate(log1p(et_dwell_msec)~aoi_posv_norm, data=subset(mdf, et_samples_count<1000), mean)
tmp = aggregate(sqrt(et_dwell_msec)~aoi_posv_norm, data=subset(mdf, et_samples_count<1000), mean)
colnames(tmp) = c('posy','etsmp')
plot(tmp)
lfit = loess(etsmp~posy, tmp)
lines(tmp$posy, predict(lfit), col='red')
abline(lm(etsmp~posy, tmp), col='blue')
#~ visualize: et samples depending on saliency
tmp = aggregate(mdf$et_samples_count, list(mdf$aoi_saliency_sum), mean)
tmp = aggregate(et_samples_count~aoi_saliency_mean, data=subset(mdf, et_samples_count<1000), mean)
colnames(tmp) = c('aoisal','etsmp')
plot(tmp)
lfit = loess(etsmp~aoisal, tmp)
lines(tmp$aoisal, predict(lfit), col='red')
abline(lm(etsmp~aoisal, tmp), col='blue')
#~ visualize: no relationship expected between saliency and vertical position
tmp = aggregate(aoi_saliency_sum~pos2, data=mdf, mean)
colnames(tmp) = c('posy','aoisal')
plot(tmp)
lfit = loess(aoisal~posy, tmp)
lines(tmp$posy, predict(lfit), col='red')
abline(lm(aoisal~posy, tmp), col='blue')
#~ visualize: relationship between predictors



#~ simple linear models
m = lm(et_samples_count ~ pos2 + aoi_saliency_sum, data=subset(mdf, et_samples_count<1000))
m = lm(et_looked_binary ~ pos2 + aoi_saliency_sum, data=subset(mdf, et_samples_count<1000))
m = lm(et_dwell_sec ~ pos2 + aoi_saliency_sum, data=subset(mdf, et_samples_count<1000))
m = lm(et_dwell_msec ~ pos2 + aoi_saliency_sum, data=subset(mdf, et_samples_count<1000))
m = lm(et_dwell_msec ~ aoi_posv_norm + aoi_salsum_norm, data=subset(mdf, et_samples_count<1000))


#~ simple mixed models
m = lmer(et_samples_count ~ pos2 + (1|subject_id), data=mdf)
m = lmer(et_samples_count ~ aoi_saliency_sum + (1|subject_id), data=mdf)
#~ m = glmer(et_looked_binary ~ pos2 + (1|subject) + (1|product), data=xdf, family="binomial")

#~ interaction
#~ saliency: x
#~ dwell: y
#~ vpos: lines (min, med max)

m = lmer(et_samples_count ~ aoi_saliency_mean + OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=xdf, subset=et_samples_count<500 & aoi_visible_binary==1)


#~ ##################################################################### 180306
#~ resume after image trial start triggers
library(readxl)
xdf = read_excel("tmp/dataset-180306.xlsx", sheet="Blad1")#na="NA"

#~ aoi_visible_binary = missing

#~ check correlation predictors


#~ == dependent 
#~ et_samples_count
#~ check for outliers


#~ == predictors
#~ saliency mean
#~ Sales_online
#~ norm vertical position
#~ option quality
xdf$aoi_saliency_mean = as.numeric(xdf$aoi_saliency_mean)
xdf$aoi_saliency_sum = as.numeric(xdf$aoi_saliency_sum)
xdf$aoi_vpos_norm = xdf$"Norm vertical position"
with(xdf, pairs(list(aoi_saliency_mean, Sales_online, OQ), gap=0, lower.panel=panel.smooth, upper.panel=panel.cor))

#~ == random factors
#~ subject
#~ aoi
xdf$subject = as.factor(xdf$subject_id)
xdf$product = paste(xdf$trial_id_task, xdf$aoi_id_product, sep='_')
xdf$product = as.factor(xdf$product)

library(lme4)
#~ continuous dependent
m = lmer(et_samples_count ~ aoi_saliency_mean + OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=xdf, subset=et_samples_count<500)
#~ binary dependent
m = glmer(et_looked_binary ~ aoi_saliency_mean + OQ + Sales_online + aoi_vpos_norm + (1|subject) + (1|product), data=xdf, subset=et_samples_count<500, family="binomial")

#~ ##################################################################### 180212
#~ resume after image trial start triggers
mdf = read.table('~/dev/python/opencv/cog-shop/csv/dataset.csv', sep='\t', header=T, strip.white=TRUE)

#~ explore dependent variables
mdf$aoi_choosen
with(mdf, hist(log(et_samples_count)))
#~ explore predictors
with(mdf, hist(aoi_saliency_mean))
with(mdf, hist(sqrt(pos2)))
with(mdf, hist(OQ))

#~ clean up
mdf$trial_id_task = as.factor(mdf$trial_id_task)
mdf$aoi_saliency_mean = as.numeric(mdf$aoi_saliency_mean)
mdf$aoi_saliency_sum = as.numeric(mdf$aoi_saliency_sum)
#~ text to columns
#~ head(strsplit(mdf$aoi_position_xywh, '\\|'))
library(stringr)
head(str_split_fixed(mdf$aoi_position_xywh, '\\|', 4))
#~ install.packages("data.table", lib="~/lib/r-cran")
library(data.table)
setDT(mdf)[, paste0("pos", 1:4) := tstrsplit(aoi_position_xywh, "|", type.convert=TRUE, fixed=TRUE)]
#~ convert sample count to binary looked (12 samples = 100 msec)
mdf$et_looked_bin = ifelse(mdf$ac=="png",0,1)

#~ diagnostics
tdf = read.table('~/dev/python/opencv/cog-shop/csv/dataset-180105.csv', sep='\t', header=T, strip.white=TRUE)
with(tdf, hist(et_missing_prop, 100))
with(mdf, hist(et_missing_prop, 100))
#~ plot proportion looked against y values
plot(tapply(mdf$et_looked_binary, mdf$pos2, mean))

with(subset(mdf, et_verify_binary==0), table(paste(trial_id_task, aoi_id_product), et_verify_binary))

length(which(mdf$et_verify_binary==1))

length(which(df$bought_produkt==1 & df$aoi_visible_binary==0))
length(which(df$bought_produkt==1 & df$et_looked_binary==0))
length(which(df$bought_produkt==1 & df$et_samples_count==0))

#~ ##################################################################### 180124
#~ resume from joost 
df = read.table("tmp/AllDataWeb180105.csv", header=T, sep="\t", stringsAsFactors=F)
str(df)
#~ 8536 obs. of  22 variables
plot(tapply(df$et_looked_binary, df$y, mean))

normalize <- function(x) {return ((x - min(x)) / (max(x) - min(x)))}

#~ check for anomalies
#~ length(which(df$bought_produkt==1)), length(which(df$et_verify_binary==1))

length(which(df$bought_produkt==1 & df$aoi_visible_binary==0))
length(which(df$bought_produkt==1 & df$et_looked_binary==0))
length(which(df$bought_produkt==1 & df$et_samples_count==0))

#~ get row by index
df[3919,]


#~ create exclusion variable
subset(data, D1 == "E" | D2 == "E")

#~ ##################################################################### 180105
#~ read dataset
library(readxl)
mdf = read_excel("tmp/AllDataWeb180105.xlsx", sheet="clean")#na="NA"
#~ clean up
mdf$trial_id_task = as.factor(mdf$trial_id_task)
mdf$aoi_saliency_mean = as.numeric(mdf$aoi_saliency_mean)
mdf$aoi_saliency_sum = as.numeric(mdf$aoi_saliency_sum)
mdf$et_missing_prop = as.numeric(mdf$et_missing_prop)


#~ explore dependent variables
mdf$aoi_choosen
with(mdf, hist(log(et_samples_count)))
#~ explore predictors
with(mdf, hist(aoi_saliency_mean))
with(mdf, hist(sqrt(pos2)))
with(mdf, hist(OQ))


library(lme4)




#~ ##################################################################### 171214
#~ read dataset
mdf = read.table('~/dev/python/opencv/cog-shop/csv/dataset.csv', sep='\t', header=T, strip.white=TRUE)
#~ try readxl
install.packages("devtools", lib="~/lib/r-cran")
devtools::install_github("hadley/readxl")
library(devtools)
with_libpaths(new='~/lib/r-cran/', install_github('hadley/readxl'))
library(readxl)
mdf = read_excel("tmp/AllDataWeb.xlsx", sheet="all")#na="NA"
#~ clean up
mdf$trial_id_task = as.factor(mdf$trial_id_task)
mdf$aoi_saliency_mean = as.numeric(mdf$aoi_saliency_mean)
mdf$aoi_saliency_sum = as.numeric(mdf$aoi_saliency_sum)
#~ text to columns
head(strsplit(mdf$aoi_position_xywh, '\\|'))
library(stringr)
head(str_split_fixed(mdf$aoi_position_xywh, '\\|', 4))
#~ install.packages("data.table", lib="~/lib/r-cran")
setDT(mdf)[, paste0("pos", 1:4) := tstrsplit(aoi_position_xywh, "|", type.convert=TRUE, fixed=TRUE)]
#~ convert sample count to binary looked (12 samples = 100 msec)
mdf$et_looked_bin = ifelse(mdf$ac=="png",0,1)

#~ #####################################################################
#~ explore dependent variables
mdf$aoi_choosen
with(mdf, hist(log(et_samples_count)))
#~ explore predictors
with(mdf, hist(aoi_saliency_mean))
with(mdf, hist(sqrt(pos2)))
with(mdf, hist(OQ))








