#~ ##################################################################### marginal histograms
#~ read coordinates
bdf = read.table('~/get/cog-shop/csv/por-xy.csv', sep='\t', header=T, strip.white=TRUE)
#~ reverse y axis for image coordinates
x = bdf$L.POR.X..px.
y = bdf$L.POR.Y..px.
#~ check distribution of gaze positions, i.e. marginal histograms
library(ggplot2)
library(gridExtra)
#~ x = adf$L.POR.X..px.
#~ y = adf$L.POR.Y..px.
#~ hist_top <- ggplot()+geom_histogram(aes(x=(..count..)/sum(..count..)))
hist_top <- ggplot()+geom_histogram(aes(x))
empty <- ggplot()+geom_point(aes(1680,1050), colour="white")+
    theme(axis.ticks=element_blank(), 
    panel.background=element_blank(), 
    axis.text.x=element_blank(), axis.text.y=element_blank(), 
    axis.title.x=element_blank(), axis.title.y=element_blank())
#~ points
scatter <- ggplot()+geom_point(aes(x, y))+
    geom_hline(yintercept=900, color="red", size=2)+
    annotate("text", x=800, y=1000, label=toString(round(length(y[y>900])/length(y), 4)), color="red", size=10)
#~ hist_right <- ggplot()+geom_histogram(aes(y=(..count..)/sum(..count..)))+coord_flip()
hist_right <- ggplot()+geom_histogram(aes(y))+coord_flip()
#~ figure
png('fig/por-xy-08.png', width=1680, height=1050)
grid.arrange(hist_top, empty, scatter, hist_right, ncol=2, nrow=2, widths=c(4, 2), heights=c(2, 4))
#~ 
dev.off()


#~ ##################################################################### inspect samples, por xy
#~ read coordinates
adf = read.table('csv/31-por-xy.csv', sep='\t', header=T, strip.white=TRUE)
#~ basic scatter plot
with(adf, plot(L.POR.X..px., L.POR.Y..px.))
#~ reverse y axis for image coordinates
x = adf$L.POR.X..px.
y = adf$L.POR.Y..px.
#~ plot(x,y, type="l", xaxt='n', yaxt='n', ylim=rev(range(y)))
#~ get first minute of samples at 120 Hz
x = adf$"L.POR.X..px."[0:(60*120)]
y = adf$"L.POR.Y..px."[0:(60*120)]
#~ plot background image
#~ install.packages("png", lib="~/lib/r-cran")
library(png)
#~ bgi = readPNG("../../python/opencv/cog-shop/img/stitch/00300.png")
bgi = readPNG("img/00300.png")
#~ figure
png('fig/por-xy-04.png', width=1680, height=1050)
#~ par(mfrow=c(1, 2))
plot(x,y, type="p", xaxt='n', yaxt='n', ylim=rev(range(y)), col="white")
axis(3)
axis(2, at=pretty(y), labels=format(pretty(y), digits=1), las=1)
lim <- par()
#~ rasterImage(bgi, lim$usr[1], lim$usr[3], lim$usr[2], lim$usr[4])
rasterImage(bgi, 0, 1050, 1680, 0)
grid()
points(x,y, col=rgb(0,0,0, 0.05))#col='magenta'
#~ compare
#~ plot(x, y)
#~ box()
#~ 
dev.off()


